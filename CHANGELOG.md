## fennel-test 0.1.60 (2025-02-16)

- Delay message creation in assertions

## fennel-test 0.1.58 (2025-02-15)

- Fix bug with `io.write` not being properly reset

## fennel-test 0.1.56 (2025-02-15)

- Linting fixes

## fennel-test 0.1.54 (2025-02-14)

- Runtime library was moved to `src/io/gitlab/andreyorst/fennel-test/init.fnl`
- Macros were moved to `src/io/gitlab/andreyorst/fennel-test/init.fnlm`
- Macros require mechanism changed to `(require-macros :io.gitlab.andreyorst.fennel-test)` from `(require-macros (doto :io.gitlab.andreyorst.fennel-test require))`

## fennel-test v0.1.52 (2025-01-12)

- Library was moved to `src/io/gitlab/andreyorst/fennel-test.fnl`
- `deps.fnl` is now provided

## fennel-test v0.1.50 (2024-08-20)

- Versioning scheme change
- Better stack traces in test failures
- Implement context reporting on failures using the `testing` macro
- Implement test skipping
- Implement more complex stats reporting with additional metrics:
  - Test time measurement
  - Assertion counting
  - Total stats of errors, warnings, skips

## fennel-test v0.0.4 (2023-08-31)

- Rework the entire library to be a self-contained single file.
- Remove the runner script.
- Provide the `run-tests` function as public API.
- Enable correlation for better locus reporting.
- Allow disabling output capturing via the `:capture-output?` output.

## fennel-test v0.0.3 (2021-10-13)

- Rework messages
- Move equality function to be a required runtime dependency
- Add basic library testing

## fennel-test v0.0.2 (2021-05-23)

- Add `.dir-locals.el` with indentation rules and highlighting.
- Always print assertion result for `assert-is` and `assert-not`.

## fennel-test v0.0.1 (2021-04-24)

Initial release of fennel-test library.

<!-- LocalWords: Cljlib namespace Memoization metatable metamethods
 -->
