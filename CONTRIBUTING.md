Please read the following document to make collaborating on the project easier for both sides.

# Reporting bugs

If you've encountered a bug, do the following:

- Check if the documentation has information about the problem you have.
  Maybe this isn't a bug, but a desired behavior.
- Check past and current issues, maybe someone has reported your problem already.
  If there's no issue, describing your problem, or there is, but it is closed, please create a new issue, and link all closed issues that relate to this problem, if any.
- Tag issue with a `BUG:` at the beginning of the issue name.

# Suggesting features and/or changes

Before suggesting a feature, please check if this feature wasn't requested before.
You can do that in the issues, by filtering issues by `FEATURE:`.
If no feature is found, please file a new issue, and tag it with a `FEATURE:` at the beginning of the issue name.

# Contributing changes

Please do.

When deciding to contribute a large amount of changes, first consider opening a `DISCUSSION:` type issue, so we can first decide if such dramatic changes are in the scope of the project.
This will save you time, in case such changes are out of the project's scope.

If you're contributing a bugfix, please open an `BUG:` issue first, unless someone already did that.
All bug-related merge requests must have linked issues with a meaningful explanation and steps for reproducing a bug.
Small fixes are also welcome and don't require filing an issue, although we may ask you to do so.

## Writing code

When writing code, consider following the existing style without applying dramatic changes to formatting unless really necessary.
For this particular project, please follow the rules as described in [Clojure Style Guide](https://github.com/bbatsov/clojure-style-guide).
If you see any inconsistencies with the style guide in the code, feel free to change these in a non-breaking way.

If you've added new functions, each one must be covered with a set of tests.

When changing existing functions make sure that all tests pass.
If some tests do not pass, make sure that these tests are written to test the function you've changed.
If not, then, perhaps, you've broke something horribly.

Before committing changes you must run tests with `deps tasks/run-tests`, and all of the tests must pass without errors.
If new test modules were added, please do not forget to add them to the [run-tests](tasks/run-tests) file.

## Writing documentation

If you've added new code, make sure it is covered not only by tests but also by documentation.
This is better done by writing documentation strings directly in the code via the docstring feature of the language.
This way this documentation can be exported to markdown later on.

Documentation files use Markdown format, as it is widely supported and can be read without any special software.
Please make sure to follow the existing style of documentation, which can be described as:

-   One sentence per line.
    This makes it easier to see changes while browsing history.
    (Note, this doesn't apply to documentation strings in the code, only to handwritten markdown files)
-   No indentation of text after headings.
    This makes little sense with a one-sentence-per-line approach anyway.
-   Amount of empty lines in text should be:
    -   Single empty lines between paragraphs.
    -   Single empty lines before and after headings.
-   Consider using spell checking.
    If you find a word not known by the dictionary, please add it to the `LocalWords` section at the bottom of the document.

## Working with Git

Check out the new branch from the project's main development branch.
If you cloned this project some time ago, consider checking if your branch has all recent changes from upstream.

When creating a merge request consider squashing your commits at merge.
You may do this manually, or use Gitlab's "Squash commits" button.

<!-- LocalWords:  bugfix docstring committing VSCode SublimeText Gitlab's LocalWords
 -->
