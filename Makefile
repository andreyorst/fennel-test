LUA ?= lua
FENNEL ?= deps --profiles dev
VERSION ?=
FNLSOURCES = src/io/gitlab/andreyorst/fennel-test/init.fnl
FNLMACROS = src/io/gitlab/andreyorst/fennel-test/init.fnlm
FNLDOCS = $(FNLMACROS) $(FNLSOURCES)
LUAEXECUTABLES ?= lua luajit
FENNELDOC := $(shell command -v fenneldoc 2>/dev/null)

.PHONY: help doc test testall $(LUAEXECUTABLES)

test:
	@echo "Testing on" $$($(LUA) -v) >&2
	@$(FENNEL) --lua $(LUA) tasks/run-tests || exit;
ifdef FENNELDOC
	@eval $($(FENNEL) --path); fenneldoc --mode check -- $(FNLDOCS) || exit
else
	@echo "" >&2
	@echo "fenneldoc is not installed" >&2
	@echo "Please install fenneldoc to check documentation during testing" >&2
	@echo "https://gitlab.com/andreyorst/fenneldoc" >&2
	@echo "" >&2
endif

testall: $(LUAEXECUTABLES)
	@$(foreach lua,$?,LUA=$(lua) make test || exit;)


doc:
ifdef FENNELDOC
	eval $($(FENNEL) --path); fenneldoc -- $(FNLMACROS) $(FNLSOURCES)
else
	@echo "" >&2
	@echo "fenneldoc is not installed" >&2
	@echo "Visit https://gitlab.com/andreyorst/fenneldoc for installation instructions" >&2
	@echo "" >&2
endif

help:
	@echo "make test -- run library tests" >&2
	@echo "make doc  -- create documentation with fenneldoc" >&2
	@echo "make help -- print this message and exit" >&2
