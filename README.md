# fennel-test

A Clojure-inspired testing library for Fennel.

This library consists of several macros that can be used to test for deep equality and generate test reports via the included test-runner.
See the [doc](.doc/fennel-test macros.md) for what's available.

This project follows the version scheme using MAJOR.MINOR.COMMIT-COUNT.
MAJOR and MINOR provide some relative indication of the size of the change, but do not follow semantic versioning.
COMMIT-COUNT is an ever-increasing counter of commits since the beginning of this repository.

## Installation

### With [deps.fnl](https://gitlab.com/andreyorst/deps.fnl)

Add this to your `deps.fnl`:

```fennel
{:deps {"https://gitlab.com/andreyorst/fennel-test"
        {:type :git :sha "416895275e4f20c1c00ba15dd0e274d1b459c533"}}}
```

## Running tests

Tests are self-contained, meaning you can just run them with the `fennel` script.
For example, given a test file `table-tests.fnl`:

```fennel
;;; table-tests.fnl
(import-macros
 {: deftest : assert-eq}
 :io.gitlab.andreyorst.fennel-test))

(deftest table-insert-test
  ;; example of a failing test
  (assert-eq
   [1 2 3]
   (table.insert [1 2] 3)))
```

The test can be run with `fennel table-tests.fnl`:

```
$ fennel table-tests.fnl
runtime error: assertion failed for expression:
(eq [1 2 3] (table.insert [1 2] 3))
 Left: [1 2 3]
Right: nil
```

Alternatively, a test runner can be used to run tests with more control.

### Test runner

This library contains a test runner function as a part of the public API, which can run tests according to the `.fennel-test` configuration file.
Usage of the test runner includes creating a fennel script `tasks/run-tests.fnl`:

```fennel
(local t (require :io.gitlab.andreyorst.fennel-test))

(local tests
  [:tests.test-module-1
   :tests.test-module-2])

(t.run-tests tests)
```

The `tests` variable holds module names that should be required for testing.
These modules must be reachable from the working directory in which the script will be invoked.
This script can then be run with `fennel tasks/run-tests.fnl` at the root of the project.

For example, here's a basic file structure of a project:

```
project
├── lib
│   └── fennel-test.fnl
├── README.md
├── .fennel-test
├── src
│   └── tested-module.fnl
├── tasks
│   └── run-tests.fnl
└── tests
    ├── test-module-1.fnl
    └── test-module-2.fnl
```

Let's look at the possible `tests/test-module-1.fnl`:

```fennel
(import-macros
  {: deftest : testing : assert-is : assert-eq}
  :io.gitlab.andreyorst.fennel-test)
(local suit (require :src.tested-module))

(deftest foo-test
  (testing "Foo works as expected"
    (assert-is (suit.foo))))

(deftest bar-test
  (testing "Bar returns expected values"
    (assert-eq {:some {:deeply [:nested :data]}}
               (suit.bar))))
```

This module defines two tests, and each one has an assertion.

Tests can be shuffled before each run with the `FENNEL_TEST_SEED` environment variable or via the `:seed` parameter in the `.fennel-test` configuration file.
By default, a semi-random seed is picked on each invocation based on the `os.time` and `os.clock` functions.

### Test reporters

There are two predefined reporters: `:dots` and `:namespaces`, which can be chosen by setting the `:reporter` key in the `.fennel-test` file.
The `:dots` reporter produces a minimalist output that only marks the start and end of each namespace, and indicates passed tests with a `.` and failed tests with an `F`.
For example, supposedly the `test-module-1` failed on the second test:

```
$ fennel tasks/run-tests.fnl
Test run at Sat Oct  7 15:28:10 2023, seed: 1696681690227

(F.)(...)

Ran 5 tests in 0.005 seconds with 13 assertions, 0 skipped, 0 warnings, 1 errors

Error in 'tests.test-module-1' in test 'bar-test':
testing: Bar returns expected values
runtime error: assertion failed for expression:
(eq {:some {:deeply ["nested" "data"]}} (suit.bar))
 Left: {:some {:deeply ["nested" "data"]}}
Right: {:some {:deeply ["nested" "values"]}}

stack traceback:
	[C]: in function 'assert'
	./tests/test-module-1.fnl:15: in function ?
	[C]: in function 'xpcall'
	./tests/test-module-1.fnl:15: in function '_25_'
	./tests/test-module-1.fnl:15: in function ?
	[C]: in function 'pcall'
	./tests/test-module-1.fnl:15: in function ?
	[C]: in function 'pcall'
	./fennel-test.fnl:644: in function 'fn1'
	./fennel-test.fnl:549: in function ?
	[C]: in function 'pcall'
	./fennel-test.fnl:552: in function 'with_no_output'
	./fennel-test.fnl:657: in function ?
	[C]: in function 'pcall'
	./fennel-test.fnl:679: in function 'run_ns_tests'
	./fennel-test.fnl:611: in function ?
	[C]: in function 'xpcall'
	/var/home/alist/.local/bin/fennel:6943: in function ?
	[C]: in ?
```

The reported result shows the stack trace of the error, and the data compared in the assertion.

Setting the `:namespaces` reporter in the `.fennel-test` file will be a bit more verbose and prints the name of each namespace, and the overall status of the namespace, which is `PASS` if all tests have passed successfully, and `FAIL` otherwise:

```
$ fennel tasks/run-tests.fnl
Test run at Sat Oct  7 15:30:42 2023, seed: 1724105134420

tests.test-module-1: FAIL
tests.test-module-2: PASS

Ran 5 tests in 0.005 seconds with 13 assertions, 0 skipped, 0 warnings, 1 errors

Error in 'tests.test-module-1' in test 'bar-test':
---- 8< ----
```

### Custom reporters

It is possible to create custom reporters using the reporter API.
Custom reporters can be written directly in the `.fennel-test` file, or in some other module and then required from the `.fennel-test` file.
The reporter itself is just a table with a predetermined set of functions:

- `(ns-start ns)` - Invoked before entering a namespace.
  Accepts the namespace's name.
- `(ns-report ns ok?)` - Invoked after leaving a namespace.
  Accepts namespace's name, and exit status, which is `true`, if all tests passed, and `false` otherwise.
- `(test-start ns test-name)` - Invoked before the test.
  Accepts current namespace's name and test's name.
- `(test-report ok? ns test-name message)` - Invoked after the test.
  Accepts test's exit status, namespace name, test name, and error message, if any.
- `(stats-report warnings errors skipped-tests assertions total-tests test-times)` - Invoked after the whole test suite has finished, and accumulated all necessary stats.
  - `warnings` - a table contains messages ready to be printed: `["some warning ...]`.
    These messages are provided by the runner and are not generated by test functions.
  - `errors` - a table contains tables of file keys:
    - `:ns` - a string with the name of the tested namespace,
    - `:test-name` - a string with the test name,
    - `:warnings` - table with strings representing warnings,
    - `:message` and error message for the test
    - `:stdout`, `:stderr` - strings with captured test output from these IO channels.
  - `skipped-tests` - a table contains information on skipped tests.
    The information comes in two forms:
    - `{:ns "namespace name" :test-name "test name" :message "optional message"}` - when a single test was skipped.
    - `{:ns "namespace name" :message "optional message" :test-count "amount of tests in the namespace (number)"}` - when all tests in the namespace were skipped at once.
  - `assertions` - a total number of executed assertions.
  - `total-tests` - a total number of tests in all namespaces, including the skipped tests.
  - `test-times` - a table with measured time of test execution.
    Each entry comes in a format `[["namespace" "test name"] "time value"]`.

For example, here's a custom reporter, that reports test errors immediately, without holding errors until the full suite is finished, as other reporters do:

```fennel
;;; .fennel-test

(local example-reporter
  {:ns-start (fn [ns] (io.write "Testing '"ns "':\n"))
   :ns-report (fn [ns ok?] (io.write ns ": " (if ok? "PASS" "FAIL") "\n---\n"))
   :test-start (fn [_ns test-name] (io.write "  " test-name ": "))
   :test-report (fn [ok? _ns _test-name msg]
                  (io.write (if ok? "PASS" "FAIL") "\n")
                  (when (not ok?) (io.write "    Reason: " msg "\n")))
   :stats-report (fn [warnings errors]
                   (if (> (length errors) 0)
                       (io.write "Test failure\n")
                       (io.write "Test passed\n")))})

{:reporter example-reporter}
```

Assigning it to the `reporter` key in the `.fennel-test` file will produce something like this:

```
Testing on Lua 5.4.4 Copyright (C) 1994-2022 Lua.org, PUC-Rio
Test run at Mon Aug 28 12:54:54 2023, seed: 1693216494187
Testing 'tests.test-module-2:
  some-test: PASS
  some-other-test: PASS
  more-tests: PASS
tests.test-module-2: PASS
---
Testing 'tests.test-module-1':
  foo-test: PASS
  bar-test: FAIL
        Reason: testing: Bar returns expected values
runtime error: assertion failed for expression:
(eq {:some {:deeply ["nested" "data"]}} (suit.bar))
 Left: {:some {:deeply ["nested" "data"]}}
Right: {:some {:deeply ["nested" "values"]}}

stack traceback:
	[C]: in function 'assert'
	./tests/test-module-1.fnl:15: in function ?
	[C]: in function 'xpcall'
	./tests/test-module-1.fnl:15: in function '_25_'
	./tests/test-module-1.fnl:15: in function ?
	[C]: in function 'pcall'
	./tests/test-module-1.fnl:15: in function ?
	[C]: in function 'pcall'
	./fennel-test.fnl:644: in function 'fn1'
	./fennel-test.fnl:549: in function ?
	[C]: in function 'pcall'
	./fennel-test.fnl:552: in function 'with_no_output'
	./fennel-test.fnl:657: in function ?
	[C]: in function 'pcall'
	./fennel-test.fnl:679: in function 'run_ns_tests'
	./fennel-test.fnl:611: in function ?
	[C]: in function 'xpcall'
	/var/home/alist/.local/bin/fennel:6943: in function ?
	[C]: in ?
tests.test-module-1: FAIL
---
Test failure
```

## Contributing

Please do.
You can report issues or feature requests at [project's Gitlab repository](https://gitlab.com/andreyorst/fennel-test).
Consider reading [contribution guidelines](https://gitlab.com/andreyorst/fennel-test/-/blob/master/CONTRIBUTING.md) beforehand.
