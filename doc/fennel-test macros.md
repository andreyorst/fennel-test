# Fennel-test macros (0.1.52-dev)
Clojure-inspired testing library for the Fennel language.

Provides a convenience macros for writing tests.

**Table of contents**

- [`deftest`](#deftest)
- [`testing`](#testing)
- [`assert-eq`](#assert-eq)
- [`assert-ne`](#assert-ne)
- [`assert-is`](#assert-is)
- [`assert-not`](#assert-not)
- [`use-fixtures`](#use-fixtures)

## `deftest`
Function signature:

```
(deftest name ...)
```

Simple way of grouping tests with `name`.

### Example
``` fennel
(deftest some-test
  ;; tests
  )
```


## `testing`
Function signature:

```
(testing description ...)
```

Wraps the test code with a `description` such that
assertions would report the testing context.

### Example

Wrapping a single test:

``` fennel
(deftest something-test
  (testing "testing something"
    ;; test body
  ))
```

Wrapping multiple tests with nested messages:

``` fennel
(deftest something-test
  (testing "testing"
    (testing "something"
      ;; test body
      )
    (testing "something else"
      ;; test body
      )))
```

When an error occurs, nested `testing` descriptions are accumulated
into a single string.

## `assert-eq`
Function signature:

```
(assert-eq expr1 expr2 msg)
```

Like `assert`, except compares results of `expr1` and `expr2` for equality.
Generates formatted message if `msg` is not set to other message.

### Example
Compare two expressions:

``` fennel
(assert-eq 1 (+ 1 2))
;; => runtime error: equality assertion failed
;; =>   Left: 1
;; =>   Right: 3
```

Deep compare values:

``` fennel
(assert-eq [1 {[2 3] [4 5 6]}] [1 {[2 3] [4 5]}])
;; => runtime error: equality assertion failed
;; =>   Left:  [1 {[2 3] [4 5 6]}]
;; =>   Right: [1 {[2 3] [4 5]}]
```

## `assert-ne`
Function signature:

```
(assert-ne expr1 expr2 msg)
```

Assert for unequality.  Like `assert`, except compares results of
`expr1` and `expr2` for unequality.  Generates formatted message if
`msg` is not set to other message.  Same as [`assert-eq`](#assert-eq).

## `assert-is`
Function signature:

```
(assert-is expr msg)
```

Assert `expr` for truth. Same as inbuilt `assert`, except generates more
  verbose message unless the `msg` is provided.

``` fennel
(assert-is (= 1 2 3))
;; => runtime error: assertion failed for (= 1 2 3)
```

## `assert-not`
Function signature:

```
(assert-not expr msg)
```

Assert `expr` for not truth. Generates more verbose message unless the
`msg` is provided.  Works the same as [`assert-is`](#assert-is).

## `use-fixtures`
Function signature:

```
(use-fixtures fixture-type fixture & fixtures)
```

Wrap test runs in a `fixture` function to perform setup and
teardown.  Using a `fixture-type` of `:each` wraps every test
individually, while `:once` wraps the whole run in a single function.
Multiple `fixtures` and fixture-types are supported in one form.

Firstures are active only when the thests are being run by the
`run-test` function.

### Example

``` fennel
(use-fixtures
 :once
 (fn [test]
   (setup1) (test) (teardown1))
 (fn [test]
   (setup2) (test) (teardown2))
 :each
 (fn [test]
   (setup3) (test) (teardown3)))
```



<!-- Generated with Fenneldoc v1.0.1
     https://gitlab.com/andreyorst/fenneldoc -->
