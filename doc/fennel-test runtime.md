# Fennel-test runtime (0.1.52-dev)
Clojure-inspired testing library for the Fennel language.

Provides a convenience functions checking in assertions and a test runner.

**Table of contents**

- [`eq`](#eq)
- [`run-tests`](#run-tests)
- [`skip-test`](#skip-test)
- [`wrap-test-error`](#wrap-test-error)

## `eq`
Function signature:

```
(eq ...)
```

Comparison function.

Accepts arbitrary amount of values, and does the deep comparison.  If
values implement `__eq` metamethod, tries to use it, by checking if
first value is equal to second value, and the second value is equal to
the first value.  If values are not equal and are tables does the deep
comparison.  Tables as keys are supported.

## `run-tests`
Function signature:

```
(run-tests modules opts)
```

Run tests in the given `modules` accordingly to the `opts` table.
Each module is loaded and inspected for tests defined with the
`deftest` macro.  Then, according to the configuration file
`.fennel-test`, the tests are shuffled, ran, and report is constructed
by the reporter specified in the config.

The `modules` argument is a sequential table of module names relative
to the script that runs the tests.

If the test module sets up fixtures with the `use-fixtures`
macro. these fixtures are used accordingly to their specs.

### Example

``` fennels
(run-tests [:tests.fixture-test :tests.equality-test])
```

## `skip-test`
Function signature:

```
(skip-test reason)
```

Calling this function inside a test or a fixture will stop the test
early and mark it as skipped. The optional `reason` argument is a
message to display in the log if the reporter is configured to do so.

## `wrap-test-error`
Function signature:

```
(wrap-test-error msg-or-fn)
```

Wraps error message `msg-or-fn` in a stack-preserving form.
Error message can be a string or a function that returns the error
string.  Only for internal use.


<!-- Generated with Fenneldoc v1.0.1
     https://gitlab.com/andreyorst/fenneldoc -->
