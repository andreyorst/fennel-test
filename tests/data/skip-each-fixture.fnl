(require-macros :io.gitlab.andreyorst.fennel-test)

(local {: skip-test} (require :io.gitlab.andreyorst.fennel-test))

(local se {:skipped? true})

(use-fixtures :once
  (fn [t]
    (t)
    (assert-is se.skipped?)))

(use-fixtures :each
  (fn [t]
    (skip-test)
    (t)))

(deftest skip-each-test
  (set se.skipped? false))
