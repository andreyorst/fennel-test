(require-macros :io.gitlab.andreyorst.fennel-test)

(local {: skip-test} (require :io.gitlab.andreyorst.fennel-test))

(local se {:skipped? true})

(use-fixtures :once
  (fn [t]
    (skip-test)
    (set se.skipped? false)
    (t)))

(deftest skip-once-test
  (assert-is se.skipped?))
