(require-macros :io.gitlab.andreyorst.fennel-test)

(local {: skip-test} (require :io.gitlab.andreyorst.fennel-test))

(local se {:skipped? true})

(use-fixtures :once
  (fn [t]
    (t)
    (assert-is se.skipped?)))

(deftest skip-test-test
  (skip-test)
  (set se.skipped? false))
