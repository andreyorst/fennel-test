(require-macros :io.gitlab.andreyorst.fennel-test)

(macro with-process-out [[name proc-expr] ...]
  `(with-open [proc# (io.popen (.. ,proc-expr " 2>&1"))]
     (let [,name (proc#:read :*a)]
       ,...)))

(deftest assertion-macros-test
  (testing "assert-is fails on nil and false"
    (with-process-out [res "deps --profiles dev tasks/run-tests data.assert-is-test"]
      (assert-is (res:find "Error in 'data.assert-is-test' in test 'assert-is-false-test'" nil true) res)
      (assert-is (res:find "Error in 'data.assert-is-test' in test 'assert-is-nil-test'" nil true) res)))
  (testing "assert-not fails on non-nil"
    (with-process-out [res "deps --profiles dev tasks/run-tests data.assert-not-test"]
      (assert-is (res:find "Error in 'data.assert-not-test' in test 'assert-not-true-test'" nil true) res)
      (assert-is (res:find "Error in 'data.assert-not-test' in test 'assert-not-value-test'" nil true) res)))
  (testing "assert-eq fails"
    (with-process-out [res "deps --profiles dev tasks/run-tests data.assert-eq-test"]
      (assert-is (res:find "Error in 'data.assert-eq-test' in test 'assert-eq-test'" nil true) res)))
  (testing "assert-ne fails"
    (with-process-out [res "deps --profiles dev tasks/run-tests data.assert-ne-test"]
      (assert-is (res:find "Error in 'data.assert-ne-test' in test 'assert-ne-test'" nil true) res))))

(deftest failure-description-test
  (testing "descriptions are present"
    (with-process-out [res "deps --profiles dev tasks/run-tests data.description-test"]
      (assert-is (res:find "testing: a simple test description" nil true) res)))
  (testing "descriptions are nested"
    (with-process-out [res "deps --profiles dev tasks/run-tests data.nested-description-test"]
      (assert-is (res:find "testing: a nested test description" nil true) res)))
  (testing "no descriptions"
    (with-process-out [res "deps --profiles dev tasks/run-tests data.no-description-test"]
      (assert-not (res:find "testing: a nested test description" nil true) res)))
  (testing "descriptions are cleaned between tests"
    (with-process-out [res "deps --profiles dev tasks/run-tests data.description-cleanup-test"]
      (assert-not (res:find "testing: a nested test description" nil true) res))))
