(require-macros :io.gitlab.andreyorst.fennel-test)

(local se {:once 0 :each 0})

(use-fixtures :once
  (fn [t]
    (set se.once (+ se.once 1))
    (t)
    (assert-eq se.each 3)))

(use-fixtures :each
  (fn [t]
    (set se.each (+ se.each 1))
    (t)))

(deftest fixture-test-1
  (assert-eq se.once 1))

(deftest fixture-test-2
  (assert-eq se.once 1))

(deftest fixture-test-3
  (assert-eq se.once 1))
