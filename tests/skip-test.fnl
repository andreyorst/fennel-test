(require-macros :io.gitlab.andreyorst.fennel-test)

(macro with-process-out [[name proc-expr] ...]
  `(with-open [proc# (io.popen (.. ,proc-expr " 2>&1"))]
     (let [,name (proc#:read :*a)]
       ,...)))

(deftest skip-test
  (testing "skip works in deftest"
    (with-process-out [res "deps --profiles dev tasks/run-tests data.skip-test"]
      (assert-is (res:find "skipped test 'skip-test-test'" nil true) res))))

(deftest skip-in-each-fixture-test
  (testing "skip works in once fixture"
    (with-process-out [res "deps --profiles dev tasks/run-tests data.skip-each-fixture"]
      (assert-is (res:find "In 'data.skip-each-fixture' skipped test 'skip-each-test'" nil true) res))))

(deftest skip-in-once-fixture-test
  (testing "skip works in once fixture"
    (with-process-out [res "deps --profiles dev tasks/run-tests data.skip-once-fixture"]
      (assert-is (res:find "In 'data.skip-once-fixture' skipped all tests" nil true) res))))
