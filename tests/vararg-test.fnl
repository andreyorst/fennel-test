(require-macros :io.gitlab.andreyorst.fennel-test)

;; This namespace can break upon compilation if (get-scope) misbehaves

(deftest vararg-test
  (testing "vararg"
    (testing "was in a function"
      (let [foo (fn [...]
                  (assert-eq 3 (select "#" ...))
                  (assert-ne 4 (select "#" ...))
                  (assert-is (= 3 (select "#" ...)))
                  (assert-not (= 4 (select "#" ...))))]
        (foo nil nil nil)))
    (testing "comes from a macro"
      (macro foo [...]
        `((fn [...] ,...) nil nil nil))
      (foo
       (assert-eq 3 (select "#" ...))
       (assert-ne 4 (select "#" ...))
       (assert-is (= 3 (select "#" ...)))
       (assert-not (= 4 (select "#" ...)))))))
